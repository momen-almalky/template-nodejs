const jwt = require('jsonwebtoken');

module.exports = function(req,res,next){
    const token = req.header('auth-token');
    if(!token) return res.status(401).send('Access Denied');

    try{
        const verified = jwt.verify(token,process.env.JWT_PASS);
        console.log(verified);
        req.user_id = verified._id;
        console.log('this is the id '.verified._id);
        next();
    }catch{
        res.status(400).send('Invalid Token');
    }

}

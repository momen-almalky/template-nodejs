const router = require('express').Router();
const UserController = require('../../controllers/Api/UserController');
const auth = require('../../middlewares/auth-header');
const upload = require('../../middlewares/uploadMiddleware');
const userController = new UserController();

router.route('/profile').get(userController.getProfile);
router.route('/profile/update').post(upload.single('profile_pic'),userController.updateProfile);

module.exports = router;

const express = require('express');
const router = express.Router();
const auth = require('../../middlewares/auth-header');
const AuthController = require('../../controllers/Api/AuthController');

// router.use('/auth', require('./auth'));

router.use('/auth', require('./auth'));
router.use('/user',auth.required, require('./user'));

// router.route('/auth/register').post(auth.optional,AuthController.register);
// router.route('/auth/login').post(auth.optional,AuthController.login);
// router.route('/auth/profile').get(auth.required,AuthController.getProfile);

module.exports = router;
const router = require('express').Router();
const AuthController = require('../../controllers/Api/AuthController');
const authController = new AuthController();

router.route('/register').post(authController.register);
router.route('/login').post(authController.login);
router.route('/code/verify').post(authController.verifyCode);
router.route('/password/forgot').post(authController.forgotPassword);
// router.get('/password/reset/:token',authController.resetPasswordView);
router.route('/password/reset/:token').get(authController.resetPasswordView).post(authController.resetPassword);

module.exports = router;

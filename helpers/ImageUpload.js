const { v4: uuidv4 } = require('uuid');
const path = require('path');
const sharp = require('sharp');

class Resize {
    constructor(folder,file_original_name) {
        this.folder = folder;
        this.file_original_name = file_original_name;
    }
    async save(buffer) {
        const filename = Resize.filename() + this.file_original_name;
        const filepath = this.filepath(filename);

        const thumbnailname = 'thumbnail_' + filename + this.file_original_name;
        const thumbnailfilepath = this.filepath(thumbnailname);

        await sharp(buffer).toFile(filepath);

        await sharp(buffer)
            .resize(300, 300, {
                fit: sharp.fit.inside,
                withoutEnlargement: true
            })
            .toFile(thumbnailfilepath);

        return filename;
    }
    static filename() {
        return `${uuidv4()}`;
    }
    filepath(filename) {
        return path.resolve(`${this.folder}/${filename}`)
    }
}
module.exports = Resize;
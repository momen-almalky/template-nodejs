const Joi = require('@hapi/joi');

const updateProfileValidation = data => {
    const schema = {
        name: Joi.string().min(6),
        email: Joi.string().min(6).email(),
        phone: Joi.string().min(6),
        password: Joi.string().min(6),
        profile_pic: Joi.binary().encoding('base64').max(6*1024*1024)
    };
    return Joi.validate(data, schema, { allowUnknown: true });
};


module.exports.updateProfileValidation = updateProfileValidation;

const Joi = require('@hapi/joi');

const registerValidation = data =>{
    const schema = {
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email(),
        phone: Joi.string().min(6).required(),
        password: Joi.string().min(6).required()
    };
    return Joi.validate(data,schema,{allowUnknown:true});
};


const loginValidation = data =>{
    const schema = {
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(4).required()
    };
    return Joi.validate(data,schema,{allowUnknown:true});
};


const verifyCodeValidation = data =>{
    const schema = {
        email: Joi.string().min(6).required().email(),
        code: Joi.string().min(4).required()
    };
    return Joi.validate(data,schema,{allowUnknown:true});
};

const forgotPassword = data =>{
    const schema = {
        email: Joi.string().min(6).required().email(),
    };
    return Joi.validate(data,schema,{allowUnknown:true});
};


module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.verifyCodeValidation = verifyCodeValidation;
module.exports.forgotPassword = forgotPassword;

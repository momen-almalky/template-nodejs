const express = require('express');
const app = express();
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');
const mongoose = require('mongoose');
const log4js = require('log4js');
const logger = require('./config/logger_helper');
// import models
require('./models/User');
// import passport after user model because passport require user model
require('./config/passport');

//import routes
const routes = require('./routes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/public', express.static('./public'));

app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
app.use(routes);
app.use(log4js.connectLogger(logger, { level: 'DEBUG' }));

dotenv.config();


app.use((req, res) => {
    res.status(404).send({
        status: 404,
        message: 'API Not Found'
    });
});


//connect to db
mongoose.connect(process.env.DB_CONNECT,
    { useNewUrlParser: true },
    () => logger.info('connected to db!')
);

app.use(express.json())

// console.log(app.routes);


app.listen(process.env.PORT || 3000, () => logger.info('Server is up'));
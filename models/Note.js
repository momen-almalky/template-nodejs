const mongoose = require('mongoose');
const noteItems = require('./NoteItems');

const noteSchema = new mongoose.Schema({
    title: {
        required: true,
        type: String,
        min: 1,
        max: 255
    },
    items: [noteItems.schema],
    date: {
        type: Date,
        default: Date.now
    }
}, { timestamps: true });

module.exports = mongoose.model('Note', noteSchema);
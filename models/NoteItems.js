const mongoose = require('mongoose');

const itemsSchema = new mongoose.Schema({
        title: {
            required: true,
            type: String,
            min: 1,
            max: 255
        },
        finished: {
            default: false,
            type: Boolean
        },
        date:{
            type: Date,
            default: Date.now
        },
        updated_on:{
            type: Date,
            default: Date.now
        }
}, { timestamps: true });

module.exports = {
    model: mongoose.model('NoteItems', itemsSchema),
    schema: itemsSchema
};
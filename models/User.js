const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const logger = require('../config/logger_helper');

const userSchema = new mongoose.Schema({

  name: {
    type: String,
    required: true,
    min: 6,
    max: 255,
    required: [true, "can't be blank"],
    index: true
  },
  email: {
    type: String,
    required: true,
    min: 6,
    max: 255,
    required: [true, "can't be blank"],
    match: [/\S+@\S+\.\S+/, 'is invalid'],
    index: true
  },
  phone: {
    type: String,
    required: true,
    min: 6,
    max: 255,
    required: [true, "can't be blank"],
    index: true
  },
  account_type: {
    type: String,
    required: true,
    min: 3,
    max: 255,
    required: [true, "can't be blank"]
  },
  active: {
    type: Boolean,
    default: true,
  },
  profile_pic: {
    type: String,
    default: '/public/uploads/images/users/profile_pic/default.png',
  },
  email_verified_at: {
    type: Date,
    default: null,
  },
  password: {
    type: String,
    required: true,
    min: 6,
    max: 1024,
    set: setPassword
  },
  verification: {
    code: {
      type: String,
      required: true,
      min: 4,
      max: 1024
    },
    expires_at: {
      type: Date,
      default: new Date(),
    }
  },
  reset_password: {
    token: {
      type: String,
      min: 4,
      max: 1024
    },
    expires_at: {
      type: Date,
      default: new Date(),
    }
  }
  // ,
  // date: {
  //   type: Date,
  //   default: Date.now
  // }
}, { timestamps: true });



userSchema.methods.toJSON = function () {
  var obj = this.toObject();
  console.log(__dirname);
  delete obj.password;
  delete obj.verification;
  delete obj.reset_password;
  obj.profile_pic_url = process.env.URL_HOST + '/public/uploads/images/users/profile_pic/' + obj.profile_pic;
  obj.thumnail_profile_pic_url = process.env.URL_HOST + '/public/uploads/images/users/profile_pic/thumbnail_' + obj.profile_pic;
  return obj
};

// userSchema.methods.setPassword = async function (password) {
//   const salt = await bcrypt.genSalt(10);
//   this.password = await bcrypt.hash(password, salt);
// };

async function setPassword(password) {
  bcrypt.genSalt(10).then(function (salt) {
    bcrypt.hash(password, salt).then(function (password) {
      return password
    });
  }).catch(function (errorMessage) {
    //error handler function is invoked 
    logger.error('info', 'exception: ');
    logger.error('error', { errorMessage });
  });

}
userSchema.methods.validatePassword = async function (password) {
  return await bcrypt.compare(password.toString(), this.password);
};


userSchema.methods.getResource = async function (user) {
  console.log(user);
  return await user.select("name email phone account_type active profile_pic");
};

userSchema.methods.generateJWT = function () {
  const expirationDate = new Date();
  expirationDate.setDate(expirationDate.getDate() + 60);

  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'secret');
};


mongoose.model('User', userSchema);
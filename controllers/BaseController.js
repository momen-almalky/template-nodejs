
class BaseController {

    static jsonResponse(res, code, message = '', data) {
        if (data) {
            return res.status(code).send({ status: code, message: message, data });
        } else {
            return res.status(code).send({ status: code, message: message });

        }
    }

    successResponse(res, message = '', data) {
        // res.type('application/json');
        return BaseController.jsonResponse(res, 200, message, data);

    }

    createdResponse(res, message = '', data) {
        // res.type('application/json');
        return BaseController.jsonResponse(res, 201, message, data);

    }

    clientError(res, message = '') {
        return BaseController.jsonResponse(res, 400, message ? message : 'Unauthorized');
    }

    unauthorized(res, message = '') {
        return BaseController.jsonResponse(res, 401, message ? message : 'Unauthorized');
    }

    unprocessed(res, message = '') {
        return BaseController.jsonResponse(res, 422, message ? message : 'Unprocessed Entity');
    }

    paymentRequired(res, message = '') {
        return BaseController.jsonResponse(res, 402, message ? message : 'Payment required');
    }

    forbidden(res, message = '') {
        return BaseController.jsonResponse(res, 403, message ? message : 'Forbidden');
    }

    notFound(res, message = '') {
        return BaseController.jsonResponse(res, 404, message ? message : 'Not found');
    }

    conflict(res, message = '') {
        return BaseController.jsonResponse(res, 409, message ? message : 'Conflict');
    }

    tooMany(res, message = '') {
        return BaseController.jsonResponse(res, 429, message ? message : 'Too many requests');
    }

    todo(res, message = '') {
        return BaseController.jsonResponse(res, 400, 'TODO');
    }

    fail(res, error) {
        console.log(error);
        return res.status(422).send({
            status: 422,
            message: error.toString()
        })
    }
}
module.exports = BaseController;

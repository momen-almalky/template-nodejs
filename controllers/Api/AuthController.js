const router = require('express').Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const dotenv = require('dotenv');
const passport = require('passport');
const logger = require('../../config/logger_helper');
const BaseController = require('../BaseController');
const email = require('../../config/email');
const crypto = require('crypto');
const async = require('async');

const { registerValidation, loginValidation, verifyCodeValidation, forgotPassword } = require('../../validations/auth')

/**
 * The App controller class where other controller inherits or
 * overrides pre defined and existing properties
 */
class AuthController extends BaseController {

    async register(req, res, next) {
        try {

            // Let's Validate the data before submiting it

            const { body } = req;
            const { error } = registerValidation(body);

            if (error) return super.unprocessed(res, error.details[0].message);

            //checking email if user is already raegister
            const user_exits = await User.findOne({ email: body.email });

            if (user_exits) return super.unprocessed(res, 'User already exits');

            //create user and save it to online mongo db
            const user_model = new User(body);
            user_model.account_type = 'client';
            user_model.verification.code = Math.floor((Math.random() * 100000) + 1);

            user_model.verification.expires_at = new Date().getDate() + 1;

            await user_model.setPassword(body.password)

            try {

                await email.send({
                    template: '../views/emails/welcome',
                    message: {
                        to: user_model.email
                    },
                    locals: user_model
                });

                const savedUser = await user_model.save();

                return super.successResponse(res, 'Welcome Abroad.');

            } catch (err) {
                return super.fail(res, err);

            }

        }
        catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return super.fail(res, 'Something Went Wrong');

        }

    }


    async login(req, res, next) {
        try {
            //validate login
            const { error } = loginValidation(req.body);
            const { body } = req;

            if (error) return super.unprocessed(res, error.details[0].message);

            const user_exit = await User.findOne({ email: body.email });

            if (!user_exit) return super.notFound(res, 'User Not Found');

            if (! await user_exit.validatePassword(body.password)) return super.notFound(res, 'Wrong Credentials');

            if (!user_exit.active) return super.forbidden(res, 'Account is deactivated by admin');

            return passport.authenticate('local', { session: false }, (err, passportUser, info) => {

                if (err) {

                    return next(err);
                }

                if (passportUser) {
                    const user = passportUser;
                    user.token = passportUser.generateJWT();

                    if (user.email_verified_at == null) {
                        return super.successResponse(res, 'Done.', { token: user.token });
                    }

                    return super.successResponse(res, 'Done.', { user: user, token: user.token });

                }

                super.fail(res, info);

            })(req, res, next);


        } catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return super.fail(res, 'Something Went Wrong');
        }
    }

    async verifyCode(req, res, next) {
        try {
            //validate login
            const { error } = verifyCodeValidation(req.body);
            const { body } = req;

            if (error) return super.unprocessed(res, error.details[0].message);

            const user_exit = await User.findOne({ email: body.email });

            if (!user_exit) return super.notFound(res, 'User Not Found');

            if (user_exit.email_verified_at) return super.successResponse(res, 'User already verified');

            if (user_exit.verification.code != body.code) return super.unprocessed(res, 'Code is wrong.');

            if (user_exit.verification.expires_at < new Date) return super.unprocessed(res, 'Code is Expired');

            user_exit.email_verified_at = new Date();
            await user_exit.save();

            return super.successResponse(res, 'Done.');
        } catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return super.fail(res, 'Something Went Wrong');
        }
    }


    forgotPassword(req, res, next) {
        try {
            //validate login
            const { error } = forgotPassword(req.body);
            const { body } = req;

            if (error) return super.unprocessed(res, error.details[0].message);

            // use of async flows

            async.waterfall([
                function (done) {
                    User.findOne({
                        email: body.email
                    }).exec(function (err, user) {
                        if (user) {
                            done(err, user);
                        } else {
                            done('User not found.');
                        }
                    });
                },
                function (user, done) {
                    // create the random token
                    crypto.randomBytes(20, function (err, buffer) {
                        var token = buffer.toString('hex');
                        done(err, user, token);
                    });
                },
                function (user, token, done) {
                    User.findByIdAndUpdate({ _id: user._id }, { reset_password: { token: token, expires_at: Date.now() + 86400000 } }, { upsert: true, new: true }).exec(function (err, new_user) {
                        done(err, token, new_user);
                    });
                },
                function (token, user, done) {
                    user.url = req.protocol + '://' + req.get('host') + '/api/auth/password/reset/' + token,
                        email.send({
                            template: '../views/emails/forgot_password',
                            message: {
                                to: user.email
                            },
                            locals: user
                        }).then(function (response) {
                            res.status(300).json({ status: 200, message: 'Kindly check your email for further instructions' })
                        }).catch(function (error) {
                            done(error)
                        })
                }
            ], function (err) {

                logger.error('info', 'exception: ');
                logger.error('error', err);
                return res.status(422).json({ status: 422, message: err });
            });


        } catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return super.fail(res, 'Something Went Wrong');
        }
    }



    async resetPasswordView(req, res, next) {
        try {

            if (!req.params.token) return super.unprocessed(res, "Unexpected Error, Token Not provided");

            const user_exit = await User.findOne({ 'reset_password.token': req.params.token, 'reset_password.expires_at': { $gt: Date.now() } });

            if (!user_exit) {
                return res.json({ message: 'Password reset token is invalid or has expired.' });
            } else {
                return res.render('emails/views/reset_password', {  token: req.params.token,layout: false });
            }
        } catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return super.fail(res, 'Something Went Wrong');
        }
    }

    async resetPassword(req, res, next) {
        try {
            //validate login
            if (!req.body.token) return super.unprocessed(res, "Unexpected Error, Token Not provided");
            if (!req.body.password) return super.unprocessed(res, "Unexpected Error, Password Not provided");
            
            const user_exit = await User.findOne({ 'reset_password.token': req.body.token, 'reset_password.expires_at': { $gt: Date.now() } });

            if (!user_exit) {
                return super.notFound(res, 'Password reset token is invalid or has expired.');
            }

            await user_exit.setPassword(req.body.password);
            user_exit.reset_password.token = undefined;
            user_exit.reset_password.expires_at = undefined;
            user_exit.reset_password = undefined;
            await user_exit.save();
            console.log(req.body);

            return res.status(200).send({ status:200,message: 'success' });
        } catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return super.fail(res, 'Something Went Wrong');
        }
    }


}



module.exports = AuthController;
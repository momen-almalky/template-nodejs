const router = require('express').Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const dotenv = require('dotenv');
const passport = require('passport');
const logger = require('../../config/logger_helper');
const BaseController = require('../BaseController');
const email = require('../../config/email');
const crypto = require('crypto');
const async = require('async');
const path = require('path');
const Resize = require('../../helpers/ImageUpload');
const { updateProfileValidation } = require('../../validations/user_profile')

/**
 * The App controller class where other controller inherits or
 * overrides pre defined and existing properties
 */
class UserController extends BaseController {

    async getProfile(req, res, next) {
        //validate login
        // const {error} = loginValidation(req.body);
        // if (error) return res.status(422).send({status:422,message:error.details[0].message});
        try {
            const { payload: { id } } = req;

            // Fetch the user by id 
            const user = await User.findOne({ _id: id }).select("name email phone account_type active");

            if (!user) {
                return res.status(404).send({ status: 404, message: 'User Not Found' });
            }

            return res.status(200).send({ status: 200, message: 'Done.', user: user });

        }
        catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return res.status(422).send({ status: 422, message: 'Something Went Wrong' });
        }

    }

    async updateProfile(req, res, next) {
        const { error } = updateProfileValidation(req.body);
        if (error) return res.status(422).send({ status: 422, message: error.details[0].message });
        try {

            const { payload: { id } } = req;
            const { body } = req;

            // Fetch the user by id 
            await User.findByIdAndUpdate(id, body);
            const user_details = await User.findById(id).select("name email phone account_type active profile_pic");

            if (req.file) {
                // res.status(401).json({ error: 'Please provide an image' });
                const imagePath = path.join(__dirname, '../../public/uploads/images/users/profile_pic');
                const fileUpload = new Resize(imagePath,req.file.originalname);
                const filename = await fileUpload.save(req.file.buffer);
                user_details.profile_pic = filename;
            }

            return res.status(200).send({ status: 200, message: 'Done.', user: user_details });

        }
        catch (ex) {
            logger.error('info', 'exception: ');
            logger.error('error', { ex });
            return res.status(422).send({ status: 422, message: 'Something Went Wrong' });
        }

    }


}



module.exports = UserController;
const { configure, getLogger } = require('log4js');

// appenders
configure({
  appenders: {
    console: { type: 'stdout', layout: { type: 'colored' } },
    dateFile: {
      type: 'dateFile',
      filename: `logs/app(`+new Date().toDateString()+`).log`,
      layout: { type: 'basic' },
      compress: true,
      daysToKeep: 14,
      keepFileExt: true
    }
  },
  categories: {
    default: { appenders: ['console', 'dateFile'], level: 'info' }
  }
});

// fetch logger and export
module.exports = getLogger();
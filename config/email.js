const Email = require('email-templates');
const email = new Email({
    message: {
        from: 'momen.dev.test@gmail.com'
    },
    send: true,
    views: {
        options: {
            extension: 'handlebars'
        }
    },
    transport: {
        host: 'smtp.gmail.com',
        port: 465,
        ssl: true,
        tls: true,
        auth: {
            user: 'momen.dev.test@gmail.com', // your Mailtrap username
            pass: '19941994Mom' //your Mailtrap password
        }
    }, preview: false
});

module.exports = email;

// email
//     .send({
//         template: 'welcome',
//         message: {
//             to: 'test@example.com'
//         },
//         locals: person
//     })
//     .then(console.log)
//     .catch(console.error);